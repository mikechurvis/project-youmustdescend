﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class CubicGrid
{
    Dictionary<Coord, ICubeElement> _gridElements;

    public struct Coord
    {
        #region Fields and Constants

        private readonly int _x;
        private readonly int _y;
        private readonly int _z;

        public static readonly Coord[] Laterals = new Coord[]
        {
            new Coord (1, 0, 0),
            new Coord (-1, 0, 0),
            new Coord (0, 1, 0),
            new Coord (0, -1, 0),
            new Coord (0, 0, 1),
            new Coord (0, 0, -1)
        };

        public static readonly Coord[] Diagonals = new Coord[]
        {
            new Coord (1, 1,    1),
            new Coord (-1, 1,   1),
            new Coord (-1, -1,  1),
            new Coord (1, -1,   1),
            new Coord (0, 1,    1),
            new Coord (0, -1,   1),
            new Coord (1, 0,    1),
            new Coord (-1, 0,   1),

            new Coord (1, 1,    0),
            new Coord (-1, 1,   0),
            new Coord (-1, -1,  0),
            new Coord (1, -1,   0),

            new Coord (1, 1,    -1),
            new Coord (-1, 1,   -1),
            new Coord (-1, -1,  -1),
            new Coord (1, -1,   -1),
            new Coord (0, 1,    -1),
            new Coord (0, -1,   -1),
            new Coord (1, 0,    -1),
            new Coord (-1, 0,   -1),
        };

        #endregion



        public Coord (int x, int y, int z)
        {
            _x = x;
            _y = y;
            _z = z;
        }



        #region Properties

        public int x
        {
            get { return _x; }
            set { this = new Coord(value, _y, _z); }
        }

        public int y
        {
            get { return _y; }
            set { this = new Coord(_x, value, _z); }
        }

        public int z
        {
            get { return _z; }
            set { this = new Coord(_x, _y, value); }
        }

        #endregion



        #region Transformations

        public static Coord Mirror (Coord coord, bool alongX, bool alongY, bool alongZ)
        {
            if (alongX && alongY && alongZ)
                return -coord;

            int newX = coord.x, 
                newY = coord.y, 
                newZ = coord.z;

            if (alongX)
                newX *= -1;
            if (alongY)
                newY *= -1;
            if (alongZ)
                newZ *= -1;

            return new Coord(newX, newY, newZ);
        }

        public static HashSet<Coord> Mirror (HashSet<Coord> coordSet, bool alongX, bool alongY, bool alongZ)
        {
            if (alongX && alongY && alongZ)
                return InverseOf(coordSet);

            HashSet<Coord> mirroredCoordSet = new HashSet<Coord>();

            foreach (Coord coord in coordSet)
                mirroredCoordSet.Add(Mirror(coord, alongX, alongY, alongZ));

            return mirroredCoordSet;
        }



        public static HashSet<Coord> InverseOf (HashSet<Coord> coordSet)
        {
            HashSet<Coord> invertedCoordSet = new HashSet<Coord>();

            foreach (Coord coord in coordSet)
            {
                invertedCoordSet.Add(-coord);
            }

            return invertedCoordSet;
        }



        public static HashSet<Coord> Offset (HashSet<Coord> coordSet, Coord byCoord)
        {
            HashSet<Coord> offsetCoordSet = new HashSet<Coord>();
            foreach (Coord coord in coordSet)
                offsetCoordSet.Add(coord + byCoord);

            return offsetCoordSet;
        }



        public static Coord RotateX (Coord coord, int quarterRevolutions)
        {
            int newY = coord.y, 
                newZ = coord.z;

            _Rotate(ref newY, ref newZ, quarterRevolutions);

            return new Coord(coord.x, newY, newZ);
        }

        public static HashSet<Coord> RotateX (HashSet<Coord> coordSet, int quarterRevolutions)
        {
            if (quarterRevolutions % 4 == 0)
                return coordSet;

            HashSet<Coord> rotatedCoordSet = new HashSet<Coord>();

            foreach (Coord coord in coordSet)
            {
                rotatedCoordSet.Add(RotateX(coord, quarterRevolutions));
            }

            return rotatedCoordSet;
        }



        public static Coord RotateY (Coord coord, int quarterRevolutions)
        {
            int newX = coord.x,
                newZ = coord.z;

            _Rotate(ref newX, ref newZ, quarterRevolutions);

            return new Coord(newX, coord.y, newZ);
        }

        public static HashSet<Coord> RotateY (HashSet<Coord> coordSet, int quarterRevolutions)
        {
            if (quarterRevolutions % 4 == 0)
                return coordSet;

            HashSet<Coord> rotatedCoordSet = new HashSet<Coord>();

            foreach (Coord coord in coordSet)
            {
                rotatedCoordSet.Add(RotateY(coord, quarterRevolutions));
            }

            return rotatedCoordSet;
        }



        public static Coord RotateZ (Coord coord, int quarterRevolutions)
        {
            int newX = coord.x,
                newY = coord.y;

            _Rotate(ref newX, ref newY, quarterRevolutions);

            return new Coord(newX, newY, coord.z);
        }

        public static HashSet<Coord> RotateZ (HashSet<Coord> coordSet, int quarterRevolutions)
        {
            if (quarterRevolutions % 4 == 0)
                return coordSet;

            HashSet<Coord> rotatedCoordSet = new HashSet<Coord>();

            foreach (Coord coord in coordSet)
            {
                rotatedCoordSet.Add(RotateZ(coord, quarterRevolutions));
            }

            return rotatedCoordSet;
        }



        private static void _Rotate (ref int a, ref int b, int quarterRevolutions)
        {
            int quarterRevMod4 = quarterRevolutions % 4,
                newValA = a,
                newValB = b;

            switch (quarterRevMod4)
            {
                case 0: break;
                case 1:
                    newValA = b;
                    newValB = -a;
                    break;
                case 2:
                    newValA = -a;
                    newValB = -b;
                    break;
                case 3:
                    newValA = -b;
                    newValB = a;
                    break;
            }
        }

        #endregion



        #region Utilities

        private static int _SumOfSquaresOfComponentsOf (Coord coord)
        {
            return (int) (Math.Pow(coord.x, 2) + Math.Pow(coord.y, 2) + Math.Pow(coord.z, 2));
        }



        /// <summary>
        ///     Returns the neighbors of a given Coord. The neighbors are those Coords immediately adjacent to (i.e. share a face with) the given Coord.
        /// If <paramref name="includeDiagonals"/> is true, then the Coords diagonal to (i.e. share an edge or a corner with) the given Coord will also 
        /// be counted as a neighbor.
        /// </summary>
        /// <param name="coord">
        ///     The given Coord.
        /// </param>
        /// <param name="includeDiagonals">
        ///     If true, the Coords immediately diagonal to the given Coord will be counted as a neighbor. This makes a "box" around the given Coord.
        /// </param>
        /// <returns>
        ///     The neighbors of the given Coord.
        /// </returns>
        public static HashSet<Coord> NeighborsOf (Coord coord, bool includeDiagonals = false)
        {
            HashSet<Coord> neighborCoordSet = new HashSet<Coord>(Laterals);

            if (includeDiagonals)
                neighborCoordSet.UnionWith(Diagonals);

            return Offset(neighborCoordSet, coord);
        }



        /// <summary>
        ///     Creates a set of Coords that is thicker than the set of Coords passed in. More technically, the expansion of a set of Coords
        /// contains the original set of Coords as well as all Coords immediately adjacent to them.
        /// </summary>
        /// <param name="coordSet">
        ///     The set of Coords used to create the expansion.
        /// </param>
        /// <param name="expandDiagonally">
        ///     If true, the Coords immediately diagonal to the given Coords will be included in the expansion. This makes the corners and
        /// edges of the expansion less "rounded".
        /// </param>
        /// <returns>
        ///     The generated expansion.
        /// </returns>
        public static HashSet<Coord> ExpansionOf (HashSet<Coord> coordSet, bool expandDiagonally = false)
        {
            HashSet<Coord> coordExpandedSet = new HashSet<Coord>(coordSet);

            foreach (Coord coord in coordSet)
            {
                coordExpandedSet.UnionWith(NeighborsOf(coord, expandDiagonally));
            }

            return coordExpandedSet;
        }



        /// <summary>
        ///     Creates a hollow shell of the set of Coords passed in. The thickness may be modified (default = 1), as well as whether
        /// diagonal expansion will be used to generate the shell.
        /// </summary>
        /// <remarks>
        ///     The shell will fit over the given coordSet with zero intersections.
        /// </remarks>
        /// <param name="coordSet">
        ///     The set of Coords used to create the shell.
        /// </param>
        /// <param name="thickness">
        ///     The lowest thickness of the shell's walls. More technically, it is the number of expansion iterations that will be used to create the shell.
        /// </param>
        /// <param name="expandDiagonally">
        ///     If true, each Coord's diagonal neighbors will be added with each expansion. This makes the corners and edges of the shell less "rounded".
        /// </param>
        /// <returns>
        ///     The generated shell.
        /// </returns>
        public static HashSet<Coord> ShellOf (HashSet<Coord> coordSet, int thickness = 1, bool expandDiagonally = false)
        {
            HashSet<Coord> coordShellSet = new HashSet<Coord>(coordSet);
            HashSet<Coord> coordExpansionBufferSet;

            for (int t = 0; t < thickness; t++)
            {
                coordExpansionBufferSet = new HashSet<Coord>(coordShellSet);
                coordShellSet = ExpansionOf(coordExpansionBufferSet, expandDiagonally);
                coordShellSet.ExceptWith(coordSet);
            }


            return coordShellSet;
        }



        /// <summary>
        ///     Creates a rectangular prism of Coords with a given size and starting position.
        /// </summary>
        /// <param name="corner">
        ///     A corner of the prism. If <paramref name="dimensions"/>'s values are all positive, it will be the bottom-left-front corner.
        /// </param>
        /// <param name="dimensions">
        ///     A Coord whose values are the width, height, and depth of the prism.
        /// </param>
        /// <returns>
        ///     The generated rectangular prism.
        /// </returns>
        public static HashSet<Coord> RectangularPrism (Coord corner, Coord dimensions)
        {
            HashSet<Coord> coordRectangleSet = new HashSet<Coord>();

            if (dimensions.x * dimensions.y * dimensions.z == 0)
                return coordRectangleSet;

            Coord oppositeCorner = dimensions + corner;
            int left = (corner.x < oppositeCorner.x) ? corner.x : oppositeCorner.x;
            int bottom = (corner.y < oppositeCorner.y) ? corner.y : oppositeCorner.y;
            int front = (corner.z < oppositeCorner.z) ? corner.z : oppositeCorner.z;

            for (int x = 0; x < Math.Abs(dimensions.x); x++)
            {
                for (int y = 0; y < Math.Abs(dimensions.y); y++)
                {
                    for (int z = 0; z < Math.Abs(dimensions.z); z++)
                    {
                        coordRectangleSet.Add(new Coord(left + x, bottom + y, front + z));
                    }
                }
            }

            return coordRectangleSet;
        }



        /// <summary>
        ///     Creates a solid sphere of Coords with the given radius and center.
        /// </summary>
        /// <param name="center">
        ///     The center of the sphere.
        /// </param>
        /// <param name="radius">
        ///     The radius of the sphere.
        /// </param>
        /// <returns>
        ///     The generated sphere of Coords.
        /// </returns>
        public static HashSet<Coord> Sphere (Coord center, float radius)
        {
            HashSet<Coord> coordSphereSet = new HashSet<Coord>();

            radius *= (radius < 0) ? -1 : 1;

            int radiusCieling = (int)Math.Ceiling(radius);
            int boundingBoxSideLength = (radiusCieling * 2) + 1;

            if (radiusCieling == 0)
                return coordSphereSet;

            Coord boundingBoxCorner = new Coord(-radiusCieling, -radiusCieling, -radiusCieling);
            Coord boundingBoxDimensions = new Coord(boundingBoxSideLength, boundingBoxSideLength, boundingBoxSideLength);

            coordSphereSet = RectangularPrism(boundingBoxCorner, boundingBoxDimensions);

            //Trim all Coords that are outside the radius of the sphere
            coordSphereSet.RemoveWhere((coord) =>
            {
                return _SumOfSquaresOfComponentsOf(coord) > Math.Pow(radius, 2);
            });

            return Offset(coordSphereSet, center);
        }

        #endregion



        #region Operators

        public static Coord operator + (Coord left, Coord right)
        {
            return new Coord(
                left._x + right._x, 
                left._y + right._y, 
                left._z + right._z);
        }

        public static Coord operator - (Coord left, Coord right)
        {
            return left + -right;
        }

        public static Coord operator - (Coord coord)
        {
            return new Coord(-coord.x, -coord.y, -coord.z);
        }

        #endregion



        public override bool Equals(object obj)
        {
            if (obj is Coord && 
                ((Coord)obj)._x == _x &&
                ((Coord)obj)._y == _y &&
                ((Coord)obj)._z == _z)
                return true;
            else return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;

                hash = hash * 23 + _x;
                hash = hash * 23 + _y;
                hash = hash * 23 + _z;

                return hash;
            }
        }
    }
}
